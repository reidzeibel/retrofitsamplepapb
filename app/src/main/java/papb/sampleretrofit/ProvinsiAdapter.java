package papb.sampleretrofit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.text.Layout;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;


// Adapter harus extend "RecyclerView.Adapter<(tipeViewHolder)>"
// (tipeViewHolder) diganti dengan class ViewHolder yang sudah dibuat
public class ProvinsiAdapter extends RecyclerView.Adapter<ProvinsiHolder> {

    // variabel untuk menyimpan daftar provinsi
    List<Provinsi> provinsiList;

    // Constructor untuk memasukkan list ke dalam variabel 'provinsiList'
    public ProvinsiAdapter(List<Provinsi> provinsiList) {
        this.provinsiList = provinsiList;
    }

    // Dipanggil untuk membuat ViewHolder
    @NonNull
    @Override
    public ProvinsiHolder onCreateViewHolder(@NonNull ViewGroup viewGroup, int i) {

        // LayoutInflater.from() memerlukan konteks
        // konteks bisa didapatkan dari viewGroup.getContext
        // inflate(x,y,z) digunakan untuk membuat View
        // x adalah Layout yang ingin ditampilkan
        // y adalah Layout tempat menempelnya x (biasanya viewGroup, sesuai dengan parameter pada onCreateViewHolder diatas)
        // z adalah apakah y menempel pada x (biasanya false)
        View view =
                LayoutInflater
                        .from(viewGroup.getContext())
                        .inflate(R.layout.item_layout,
                                viewGroup,
                                false);

        // buat ViewHolder dari view yang sudah dibuat
        return new ProvinsiHolder(view);
    }

    // Dipanggil untuk menaruh item dari list kedalam ViewHolder
    // ProvinsiHolder adalah viewHolder yang diextend pada pembuatan adapter (cek diatas)
    // int i adalah posisi view saat ini (jumlah sesuai dengan ukuran list)
    // i bisa digunakan untuk mengambil item pada posisi yang sama dari daftar
    @Override
    public void onBindViewHolder(@NonNull ProvinsiHolder provinsiHolder, int i) {
        // ambil sebuah objek dari daftar
        Provinsi provinsi = provinsiList.get(i);

        // pasangkan objek pada viewholder
        provinsiHolder.nama.setText(provinsi.nama);
    }


    // digunakan untuk menentukan jumlah item pada recyclerview
    @Override
    public int getItemCount() {
        return provinsiList.size();
    }
}
