package papb.sampleretrofit;

import retrofit2.Call;
import retrofit2.http.GET;

// deklarasikan semua endpoint API disini
public interface Service {

    // Metode : Get
    // endpoint : provinsi (ini akan ditambahkan ke BaseUrl di RetrofitInstance)
    // Call<ObjekRespondariServer> namaMetode();
    @GET("provinsi")
    Call<ResponServer> getProvinsi();

}
