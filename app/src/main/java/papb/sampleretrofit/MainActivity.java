package papb.sampleretrofit;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.widget.Toast;

import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // inisialisasi RecyclerView, sambungkan dengan recyclerView di XML
        final RecyclerView recyclerView = findViewById(R.id.recycler);

        // siapkan layout manager
        recyclerView.setLayoutManager(new LinearLayoutManager(this));

        // panggil retrofit
        // RetrofitInstance.get() untuk mengambil Retrofitnya
        // getProvinsi() untuk mengambil list Provinsi
        // enqueue() untuk melakukan pemanggilan secara Async
        // pada enqueue() ditambahkan Callback sesuai dengan tipe data yang akan diambil
        // (dalam kasus ini, "ResponServer")
        RetrofitInstance.get().getProvinsi().enqueue(
                new Callback<ResponServer>() {
                    // dipanggil jika sukses mendapatkan respon dari server
                    @Override
                    public void onResponse(Call<ResponServer> call,
                                           Response<ResponServer> response) {

                        // cek apakah pemanggilan sukses atau gagal
                        if (response.isSuccessful()) {
                            //respon sukses, ambil objek "ResponServer" pada `body` response
                            ResponServer responServer = response.body();

                            // ambil list provinsi dari responServer
                            List<Provinsi> provinsiList = responServer.semuaprovinsi;

                            // tambahkan list provinsi ke adapter
                            ProvinsiAdapter provinsiAdapter = new ProvinsiAdapter(provinsiList);

                            // set Adapter ke RecyclerView
                            recyclerView.setAdapter(provinsiAdapter);

                        } else {
                            //respon gagal, tampilkan log

                            Log.e("####", "Respon gagal");
                        }
                    }

                    // dipanggil jika gagal mendapatkan respon dari server
                    @Override
                    public void onFailure(Call<ResponServer> call,
                                          Throwable t) {
                        Log.e("### Failure", t.getMessage());
                    }
                }
        );
    }
}
