package papb.sampleretrofit;

import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.TextView;


// Kelas ViewHolder untuk menampilkan item pada Adapter
// Harus extends RecyclerView.ViewHolder

public class ProvinsiHolder extends RecyclerView.ViewHolder {

    // Deklarasikan View sesuai dengan layout yang ada
    // View ini untuk menampilkan informasi dari data pada list
    // misalnya jika kita ingin menampilkan nama provinsi, maka deklarasikan TextView nama
    // jika ingin menampilkan harga barang, tambahkan textview sesuai kebutuhan
    // bisa juga menggunakan ImageView jika ingin menampilkan gambar
    TextView nama;

    // Constructor, bentuknya harus "public NamaKelas(View namaVariabel)"
    // harus ada "super(namaVariabel)" di baris pertama sebagai syarat dibuatnya suatu ViewHolder
    // inisialisasikan semua View yang dideklarasikan diatas pada constructor
    // caranya dengan menggunakan "namaVariabel.findViewById(R.id.namaDiXml)"
    public ProvinsiHolder(@NonNull View itemView) {
        super(itemView);
        nama = itemView.findViewById(R.id.nama);
    }
}
