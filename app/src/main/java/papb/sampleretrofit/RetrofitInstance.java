package papb.sampleretrofit;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class RetrofitInstance {

    private static Retrofit retrofitInstance;
    private static Service service;

    // inisialisasi Retrofit secara Lazy Initialization (https://stackoverflow.com/questions/8297705/how-to-implement-thread-safe-lazy-initialization)
    private static Retrofit getInstance() {
        if (retrofitInstance == null) {

            // buat instance retrofit
            // baseUrl harus diakhiri dengan "/"
            retrofitInstance = new Retrofit.Builder()
                    .baseUrl("http://dev.farizdotid.com/api/daerahindonesia/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofitInstance;
    }

    // inisialisasi Service secara Lazy Initialization (https://stackoverflow.com/questions/8297705/how-to-implement-thread-safe-lazy-initialization)
    public static Service get() {
        if (service == null) {
            // buat instance service
            service = getInstance().create(Service.class);
        }
        return service;
    }
}
